var imagesDiv = document.getElementById("images");
var imageIndex = 0;

var imageDescription = [];
imageDescription[0] = "";
imageDescription[1] = "Vår första dejt";
imageDescription[2] = "Vår andra glassbild till Pontus";
imageDescription[3] = "Gräsmys";
imageDescription[4] = "Vår första provrumsdejt";
imageDescription[5] = "Japan-turister";
imageDescription[6] = "Två Zapata-djur på en parkering";
imageDescription[7] = "Vackra höstbilder";
imageDescription[8] = "Glasögon-modeller";
imageDescription[9] = "Sevärdhetsjakt i Insjön";
imageDescription[10] = "Vår första Lisebergsdejt";
imageDescription[11] = "Puss!";
imageDescription[12] = "Romantisk födelsedagspicnic";
imageDescription[13] = "Romantisk weekend på Rånäs slott";
imageDescription[14] = "En mysig vandringsdejt";
imageDescription[15] = "Fiji!";
imageDescription[16] = "Picnic på Tjolöholms slott";
imageDescription[17] = "En midsommardejt";
imageDescription[18] = "";

function updateImages() {
  imagesDiv.innerHTML = "<p>" + imageDescription[imageIndex] + "</p>";

  if (imageIndex === 0) {
    imagesDiv.innerHTML += "<img style='width: 500px;' src='images/album-tangerine.png' />";
  }

  else if (imageIndex === 1) {
    imagesDiv.innerHTML += "<img style='width: 700px;' src='images/2/1.jpg' />";
  }

  else if (imageIndex === 2) {
    imagesDiv.innerHTML += "<img style='width: 700px;' src='images/2/2.jpg' />";
  }

  else if (imageIndex === 3) {
    imagesDiv.innerHTML += "<img style='width: 700px;' src='images/2/3.jpg' />";
  }

  else if (imageIndex === 4) {
    imagesDiv.innerHTML += "<img style='height: 500px;' src='images/2/4-1.jpg' />";
    imagesDiv.innerHTML += "<img style='height: 500px;' src='images/2/4-2.jpg' />";
  }

  else if (imageIndex === 5) {
    imagesDiv.innerHTML += "<img style='height: 500px;' src='images/2/5-1.jpg' />";
    imagesDiv.innerHTML += "<img style='height: 500px;' src='images/2/5-2.jpg' />";
  }

  else if (imageIndex === 6) {
    imagesDiv.innerHTML += "<img style='width: 700px;' src='images/2/6.jpg' />";
  }

  else if (imageIndex === 7) {
    imagesDiv.innerHTML += "<img style='width: 550px;' src='images/2/7-1.jpg' />";
    imagesDiv.innerHTML += "<img style='height: 500px;' src='images/2/7-2.jpg' />";
  }

  else if (imageIndex === 8) {
    imagesDiv.innerHTML += "<img style='width: 550px;' src='images/2/8-1.jpg' />";
    imagesDiv.innerHTML += "<img style='width: 550px;' src='images/2/8-22.png' />";
  }

  else if (imageIndex === 9) {
    imagesDiv.innerHTML += "<img style='width: 500px;' src='images/2/9-1.jpg' />";
    imagesDiv.innerHTML += "<img style='width: 500px;' src='images/2/9-2.jpg' />";
  }

  else if (imageIndex === 10) {
    imagesDiv.innerHTML += "<img style='width: 700px;' src='images/2/10.jpg' />";
  }

  else if (imageIndex === 11) {
    imagesDiv.innerHTML += "<img style='width: 550px;' src='images/2/11-1.jpg' />";
    imagesDiv.innerHTML += "<img style='width: 550px;' src='images/2/11-22.png' />";
  }

  else if (imageIndex === 12) {
    imagesDiv.innerHTML += "<img style='width: 500px;' src='images/2/12-2.jpg' />";
    imagesDiv.innerHTML += "<img style='width: 400px;' src='images/2/12-4.jpg' />";
  }

  //else if (imageIndex === 12) {
  //  imagesDiv.innerHTML += "<img src='images/2/13.jpg' />";
  //}

  else if (imageIndex === 13) {
    imagesDiv.innerHTML += "<img style='width: 500px;' src='images/2/14-1.png' />";
    imagesDiv.innerHTML += "<img style='width: 400px;' src='images/2/14-2.jpg' />";
    imagesDiv.innerHTML += "<img style='width: 500px;' src='images/2/14-3.jpg' />";
    imagesDiv.innerHTML += "<img style='width: 500px;' src='images/2/14-4.jpg' />";
//    imagesDiv.innerHTML += "<img style='width: 700px;' src='images/2/14-5.jpg' />";
    imagesDiv.innerHTML += "<img style='width: 700px;' src='images/2/14-6.jpg' />";
  }

  else if (imageIndex === 14) {
    imagesDiv.innerHTML += "<img style='width: 700px;' src='images/2/15-1.jpg' />";
    imagesDiv.innerHTML += "<img style='width: 400px;' src='images/2/15-2.jpg' />";
    imagesDiv.innerHTML += "<img style='width: 700px;' src='images/2/15-3.jpg' />";
  }

  else if (imageIndex === 15) {
    imagesDiv.innerHTML += "<img style='width: 420px;' src='images/2/16-1.jpg' />";
    imagesDiv.innerHTML += "<img style='width: 600px;' src='images/2/16-2.jpg' />";
    imagesDiv.innerHTML += "<img style='width: 550px;' src='images/2/16-3.jpg' />";
    imagesDiv.innerHTML += "<img style='width: 550px;' src='images/2/16-4.jpg' />";
  }

  else if (imageIndex === 16) {
    imagesDiv.innerHTML += "<img style='height: 500px;' src='images/2/17-1.jpg' />";
    imagesDiv.innerHTML += "<img style='height: 500px;' src='images/2/17-2.jpg' />";
    imagesDiv.innerHTML += "<img style='height: 500px;' src='images/2/17-32.png' />";
    imagesDiv.innerHTML += "<img style='height: 500px;' src='images/2/17-4.jpg' />";
    imagesDiv.innerHTML += "<img style='width: 600px;' src='images/2/17-6.jpg' />";
    imagesDiv.innerHTML += "<img style='height: 500px;' src='images/2/17-62.png' />";
    imagesDiv.innerHTML += "<img style='height: 500px;' src='images/2/17-7.png' />";
    imagesDiv.innerHTML += "<img style='height: 500px;' src='images/2/17-8.jpg' />";
  }

  else if (imageIndex === 17) {
    imagesDiv.innerHTML += "<img style='height: 600px;' src='images/2/18-3.jpg' />";
    imagesDiv.innerHTML += "<img style='height: 600px;' src='images/2/18-2.jpg' />";
  }

  else if (imageIndex === 18) {
    imagesDiv.innerHTML += "<img style='width: 500px;' src='images/albumback-tangerinesmall.png' />";
  }
  
  

  imageIndex++;
}

updateImages();